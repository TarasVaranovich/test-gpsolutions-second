package task2;

import java.math.BigInteger;
import java.math.BigDecimal;
import java.lang.ArithmeticException;
import java.math.MathContext;

public class CalculationsFunctionsTest {
	//проверить результат так же про тестить integer простым умножением
	//NaN if devision by null
	public static void main(String[] args) {
		
		String op1 = "-1999999999";//"12345.56789";
		String op2 = "121314123455678911";//"12131412345567891011121314.12345561314";
		expressionTypeIdentification(op1, op2);
		System.out.println(Double.valueOf(Math.pow(Double.valueOf(op1), Double.valueOf(op2))));
		
		System.out.println("Test result overflow after operations performing:");
		System.out.println("Double result overflow:");
		Double op1d = Double.MAX_VALUE;
		Double op2d = Double.MAX_VALUE;
		System.out.println(Double.isInfinite(op1d * op2d));
		System.out.println(Double.isInfinite(Math.pow(op1d, op2d)));
		Double op1da = Double.MAX_VALUE;
		Double op2da = Double.MAX_VALUE;
		System.out.println(Double.isInfinite(op1da + op2da));
		System.out.println(Double.isInfinite(op1da -op2da - op2da - op1da));
		System.out.println("Integer result overflow:");
		Integer op1i = Integer.MAX_VALUE;
		Integer op2i = Integer.MAX_VALUE;
		System.out.println((long)op1i * op2i > Integer.MAX_VALUE);
		System.out.println((long)op1i + op2i > Integer.MAX_VALUE);
		Integer op1n = Integer.MIN_VALUE;
		Integer op2n = Integer.MIN_VALUE;
		System.out.println((long)op1n * op2n > Integer.MAX_VALUE);
		System.out.println((long)op1n + op2n < Integer.MIN_VALUE);
		//add or > MAX or < MIN
		System.out.println("Addition test:");
		System.out.println(addition("23","45"));
		System.out.println(addition(String.valueOf(Integer.MAX_VALUE),String.valueOf(Integer.MAX_VALUE)));
		System.out.println(addition("1234567891155","1234567891155"));
		System.out.println(addition("123.55","1.5"));
		System.out.println(addition(String.valueOf(Double.MAX_VALUE),String.valueOf(Double.MAX_VALUE)));
		String dop1 = "999999999999999999999999999999999999999999999999999999999999999999999999999999999"
				+"999999999999999999999999999999999999999999999999999999999999999999999999999999999"
				+ "9999999999999999999999999999999999999999999999999999999999999999999999999999999999999"
				+ "999999999999999999999999999999999999999999999999999999999999999999999.99999999999999999";
		String dop2 = "99999999999999999999999999999999999999999999999.99999999999999999";
		System.out.println(addition(dop1,dop2));
		System.out.println("Multiplication test:");
		System.out.println(multiplication("20","15"));
		System.out.println(multiplication(String.valueOf(Integer.MAX_VALUE),String.valueOf(Integer.MAX_VALUE)));
		System.out.println(multiplication("1234567891155","1234567891155"));
		System.out.println(multiplication("0.5","1.5"));
		System.out.println(multiplication(String.valueOf(Double.MAX_VALUE),String.valueOf(Double.MAX_VALUE)));
		System.out.println(multiplication(dop1,dop2));
		System.out.println("Exponentiation test:");
		System.out.println(exponentiation("20","2"));
		System.out.println(exponentiation(String.valueOf(Integer.MAX_VALUE),String.valueOf(Integer.MAX_VALUE)));
		System.out.println(exponentiation("1234567891155","1234567891155"));
		System.out.println(exponentiation("3.5","-1"));
		System.out.println(exponentiation(String.valueOf(Double.MAX_VALUE),String.valueOf(Double.MAX_VALUE)));
		//System.out.println("Last:");
		//System.out.println(exponentiation("1234567891155",String.valueOf(Integer.MAX_VALUE)));
		//System.out.println(exponentiation(dop1,"1234"));
		System.out.println("Devision test:");
		System.out.println(division("4","2"));
		System.out.println(division("4","3"));
		System.out.println(division("1","3"));
		System.out.println(division("1234567891155","1234567891155"));
		System.out.println(division("1234567891155","2234567891155"));
		System.out.println(division("1.25","0.7"));
		System.out.println(division(String.valueOf(Integer.MAX_VALUE),String.valueOf(Integer.MAX_VALUE)));
		System.out.println(division(String.valueOf(Double.MAX_VALUE),String.valueOf(Double.MIN_VALUE)));
		System.out.println("Devision test check if zero.");
		System.out.println(division("4","0"));
		System.out.println(division("1.25","0.0"));
		System.out.println(division("1234567891155","0000000000"));
		System.out.println(division(dop1,"0000000000.0000000000"));
		System.out.println("Subtraction test:");
		System.out.println(subtraction("23","45"));
		System.out.println(subtraction(String.valueOf(Integer.MIN_VALUE),String.valueOf(Integer.MAX_VALUE)));
		System.out.println(subtraction("100050000000","500005000000"));
		System.out.println(subtraction("123.55","1.5"));
		System.out.println(subtraction(String.valueOf(Double.MAX_VALUE),String.valueOf(Double.MAX_VALUE*(-1))));
		System.out.println(subtraction(dop2,dop1));
	}
	
	//1 - integer
	//2 - big integer
	//3 - double
	//4 - big decimal
	public static int expressionTypeIdentification(String operand1, String operand2){
		//minus sign will transformed
		if(!(operand1.contains(".") || operand2.contains("."))) {
			
			if(isInteger(operand1) && isInteger(operand2)){
				
				return 1;
				
			} else {

				return 2;
			}		
		} else {
					
			if(!(Double.valueOf(operand1).isInfinite() || Double.valueOf(operand2).isInfinite())){
				
				return 3;
				
			} else {
				System.out.println("super big double.");
				return 4;			
			}
		}
	}
	
	public static boolean isInteger(String string){
		
		try {
			
	        Integer.valueOf(string);
	        
	        return true;
	        
	    } catch (NumberFormatException e) {
	    	
	        return false;
	    }
	}
	
	//+
	public static String addition(String operand1, String operand2){	
		
		String result = "";
		
		switch(expressionTypeIdentification(operand1, operand2)){
		//integer
		case 1:
			
			long addResult = (long)Integer.valueOf(operand1) + Integer.valueOf(operand2);
			
			if(!(addResult > Integer.MAX_VALUE || addResult < Integer.MIN_VALUE)) {
				
				result = String.valueOf(addResult);
				
				break;
				
			} 
		//big integer	
		case 2: 
			
			BigInteger operand1BigInt = new BigInteger(operand1);
			BigInteger operand2BigInt = new BigInteger(operand2);
			BigInteger resultBigInt = operand1BigInt.add(operand2BigInt);
			
			result = resultBigInt.toString();
			
			break;
		//double	
		case 3:
			
			Double resultDouble = Double.valueOf(operand1) + Double.valueOf(operand2);
			if(!Double.isInfinite(resultDouble)) {
				
				result = resultDouble.toString();
				
				break;
			} 
		//big decimal	
		case 4: 
			
			BigDecimal operand1BigDec = new BigDecimal(operand1); 
			BigDecimal operand2BigDec = new BigDecimal(operand2); 
			BigDecimal resultBigDec = operand1BigDec.add(operand2BigDec);
			
			result = resultBigDec.toString();
			
			break;	
			
		default:
			
			System.out.println("Error in expression type identification.");
			
			break;
		}
		
		return result;
	}
	
	//-
	public static String subtraction(String operand1, String operand2){
		
		String result = "";
		
		switch(expressionTypeIdentification(operand1, operand2)){
		//integer
		case 1:
			
			long subtrResult = (long)Integer.valueOf(operand1) - Integer.valueOf(operand2);
			
			if(!(subtrResult > Integer.MAX_VALUE || subtrResult < Integer.MIN_VALUE)) {
				
				result = String.valueOf(subtrResult);
				
				break;
				
			} 
		//big integer	
		case 2: 
			
			BigInteger operand1BigInt = new BigInteger(operand1);
			BigInteger operand2BigInt = new BigInteger(operand2);
			BigInteger resultBigInt = operand1BigInt.subtract(operand2BigInt);
			
			result = resultBigInt.toString();
			
			break;
		//double	
		case 3:
			
			Double resultDouble = Double.valueOf(operand1) - Double.valueOf(operand2);
			if(!Double.isInfinite(resultDouble)) {
				
				result = resultDouble.toString();
				
				break;
			} 
		//big decimal	
		case 4: 
			
			BigDecimal operand1BigDec = new BigDecimal(operand1); 
			BigDecimal operand2BigDec = new BigDecimal(operand2); 
			BigDecimal resultBigDec = operand1BigDec.subtract(operand2BigDec);
			
			result = resultBigDec.toString();
			
			break;	
			
		default:
			
			System.out.println("Error in expression type identification.");
			
			break;
		}
		
		return result;
	}
	
	//*
	public static String multiplication(String operand1, String operand2) {
		
		String result = "";
		
		switch(expressionTypeIdentification(operand1, operand2)){
		//integer
		case 1:
			
			long addResult = (long)Integer.valueOf(operand1) * Integer.valueOf(operand2);
			
			if(!(addResult > Integer.MAX_VALUE || addResult < Integer.MIN_VALUE)) {
				
				result = String.valueOf(addResult);
				
				break;
				
			} 
		//big integer	
		case 2: 
			
			BigInteger operand1BigInt = new BigInteger(operand1);
			BigInteger operand2BigInt = new BigInteger(operand2);
			BigInteger resultBigInt = operand1BigInt.multiply(operand2BigInt);
			
			result = resultBigInt.toString();
			
			break;
		//double	
		case 3:
			
			Double resultDouble = Double.valueOf(operand1) * Double.valueOf(operand2);
			if(!Double.isInfinite(resultDouble)) {
				
				result = resultDouble.toString();
				
				break;
			} 
		//big decimal	
		case 4: 
			
			BigDecimal operand1BigDec = new BigDecimal(operand1); 
			BigDecimal operand2BigDec = new BigDecimal(operand2); 
			BigDecimal resultBigDec = operand1BigDec.multiply(operand2BigDec);
			
			result = resultBigDec.toString();
			
			break;	
			
		default:
			
			System.out.println("Error in expression type identification.");
			
			break;
		}
		
		return result;
	}
	
	// \
	public static String division(String operand1, String operand2) {
		
		String result = "";
		
		try{
			switch(expressionTypeIdentification(operand1, operand2)){
			//integer
			//isInteger("2.0") return false - next operation will create double for this number
			case 1:
	
				Integer operand1Int = Integer.valueOf(operand1);
				Integer operand2Int = Integer.valueOf(operand2);
				if(operand2Int != 0) {
					if(operand1Int % operand2Int == 0) {
						
						Integer devResult =  operand1Int/operand2Int;
						result = String.valueOf(devResult);
							
					} else {
							
						Double devResult =  (double)operand1Int/operand2Int;
						result = String.valueOf(devResult);
					}								
					break;
				} else {
					
					System.out.println("Devision by zero.");
					
					return "";
				}					
			//big integer	
			case 2: 
					
				BigInteger operand1BigInt = new BigInteger(operand1);
				BigInteger operand2BigInt = new BigInteger(operand2);
				if(!(operand2BigInt.compareTo(BigInteger.ZERO) == 0)){
					
					if(operand1BigInt.remainder(operand2BigInt).compareTo(BigInteger.ZERO) == 0){
						
						BigInteger resultBigInt = operand1BigInt.divide(operand2BigInt);	
						result = resultBigInt.toString();
							
					} else {
							
						BigDecimal operand1BigDec = new BigDecimal(operand1);
						BigDecimal operand2BigDec = new BigDecimal(operand2);
						// can be result 1/3 = 0.333(3) and must restrict precision
						BigDecimal resultBigDec = operand1BigDec.divide(operand2BigDec, MathContext.DECIMAL128);
							
						result = resultBigDec.toString();
					}		
				} else {
					
					System.out.println("Devision by zero.");
					
					return "";
				}	
											
				break;
			//double	
			case 3:
				
				Double operand1Double = Double.valueOf(operand1);
				Double operand2Double = Double.valueOf(operand2);
				
				if(operand2Double != 0){
					
					Double resultDouble = operand1Double/operand2Double;
					
					if(!Double.isInfinite(resultDouble)) { // can be infinity because can divide by value 0..1
							
						result = resultDouble.toString();
							
						break;
					} 
				}else {
					
					System.out.println("Devision by zero.");
					
					return "";
				}	
				
			//big decimal	
			case 4: 
					
				BigDecimal operand1BigDec = new BigDecimal(operand1); 
				BigDecimal operand2BigDec = new BigDecimal(operand2); 
				
				if(!(operand2BigDec.compareTo(BigDecimal.ZERO) == 0)){
					
					// can be result 1/3 = 0.333(3) and must restrict precision
					BigDecimal resultBigDec = operand1BigDec.divide(operand2BigDec, MathContext.DECIMAL128);
						
					result = resultBigDec.toString();
						
					break;	
					
				} else {
					
					System.out.println("Devision by zero.");
					
					return "";
				}
				
					
			default:
					
				System.out.println("Error in expression type identification.");
					
				break;
			}
		} catch(ArithmeticException e){
				
			System.out.println("Error: devision by zero.");
			e.printStackTrace();
			return "";
		}		
		
		return result;
	}
	
	//^
	public static String exponentiation(String operand1, String operand2) {
		
		String result = "";
		
		if(isInteger(operand2)) {
			
			switch(expressionTypeIdentification(operand1, operand2)){
		
			//big integer	
			case 2: 
				
				BigInteger operand1BigInt = new BigInteger(operand1);
				Integer operand2Int = Integer.valueOf(operand2);
				//ДОЛГИЕ РАСЧЕТЫ - ДАТЬ ЮЗЕРУ ВОЗИЛЖНОСТЬ ПРЕРВАТЬ ПРОГУ
				try {
					
					BigInteger resultBigInt = operand1BigInt.pow(operand2Int); // check arithmetic exception			
					result = resultBigInt.toString();
					
				}catch(ArithmeticException e) {
					
					System.out.println("Problems with calculation. JVM restrictions. Presumably to large exponent value.");
					
					return "";
				}			
				break;
			//integer or double because Math.pow require double arguments and return double result
			case 1:
			case 3:
				
				Double resultDouble = Math.pow(Double.valueOf(operand1), Double.valueOf(operand2));
				if(!Double.isInfinite(resultDouble)) {
					
					result = resultDouble.toString();
					
					break;
				} 
			//big decimal	
			case 4: 
				
				BigDecimal operand1BigDec = new BigDecimal(operand1); 
				Integer operand2IntD = Integer.valueOf(operand2); 
					
				try {
						
					BigDecimal resultBigDec = operand1BigDec.pow(operand2IntD);
					result = resultBigDec.toString();	
						
				}catch(ArithmeticException e) {
						
					System.out.println("Problems with calculation. JVM restrictions. Presumably to large exponent value.");
					System.out.println("No need to calculate pow(n) if result will over/underflow.(c)");
					System.out.println("Suggest restrict exponent in range 0..999999999 for this case (Big decimal).");
						
					return "";
				}				
				
				break;	
				
			default:
				
				System.out.println("Error in expression type identification.");
				
				break;
			}
		} else {
			
			System.out.println("Error in expression operand " + operand2);
			System.out.println("Exponent must be integer in range -2147483648 .. 2147483647).");
		}
			
		return result;
	}
}
