package task2;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;
import java.util.Map;
import java.util.HashMap;
import java.lang.StringBuilder;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class task2 {
	
	public static final String floatingPointRegexp = "([0-9]*[.])?[0-9]+";
	public static final String splitStringRegexp = "((?<=\\+)|(?=\\+))" +
													"|((?<=\\-)|(?=\\-))" +
													"|((?<=\\*)|(?=\\*))" +
													"|((?<=\\/)|(?=\\/))" +
													"|((?<=\\^)|(?=\\^))" +
													"|((?<=\\()|(?=\\())" +
													"|((?<=\\))|(?=\\)))";
	
	public static void main(String[] args) {
	
		System.out.println("Task2 program.");
		Scanner scanner = new Scanner(System.in);
		String expressionString = "";
		System.out.println("Input expression for calculation:");
		expressionString = scanner.nextLine();
		
		
		System.out.println("Data validation...");
		expressionString = validateStringExpression(expressionString);	

		if(expressionString.length() !=0){
			
			Queue<String> infixNotationQueue = getInfixNotation(expressionString);
			System.out.println("Transform expression...");
			Queue<String> RPNQueue = shuntingYard(infixNotationQueue);	

			System.out.println("Calculating result...");
			String result = "";
			
			if(RPNQueue.size() != 0){
				
				result = calculateRPN(RPNQueue);
				
			} else {
				
				System.out.println("Program terminated because of expression convertion.");
			}		
			
			if(result.length() != 0) {
				
				System.out.println("Result:" + result);
				
			} else {
				
				System.out.println("Program terminated because of arithmetical failure.");
			}
			
			
		} else {
			
			System.out.println("Program terminated because of data format failure.");
		}
		
		System.out.println("Exit from Task2 program.");
		System.exit(0);
	}
	
	public static String validateStringExpression(String expressionString){
		
		//check if string contains unacceptable symbols
		if(!expressionString.matches("[\\d\\s?\\-?\\+?\\*?\\^?\\/?\\.?\\,?\\(?\\)?]+")){
			
			System.out.println("Unacceptable symbols in expression string.");
			
			return "";
		}
		
		//check if exists spaces in numbers 
		if(expressionString.matches(".*[.,\\d][\\s]+[.,\\d].*")) {
					
			System.out.println("Remove excess spaces in numbers.");
					
			return "";
		}
		
		//transform expression string to opportunity write operators "^"/"*"/"\" without brackets
		if(expressionString.matches(".*[\\*\\^\\/] *[\\-\\+].*")) {
			
			Pattern pattern = Pattern.compile("[\\*\\^\\/] *[\\-\\+] *" + floatingPointRegexp);
			
			while (true) {
				
				Matcher matcher = pattern.matcher(expressionString);
				
				if(matcher.find()){
					
				     expressionString = new StringBuilder(expressionString).insert(matcher.start() + 1, "(0").toString();
				     expressionString = new StringBuilder(expressionString).insert(matcher.end()+2, ")").toString();
				     
				} else {
					
					break;
				}		    	       
		    }
		}
		
		//check if operands with failure
		if(expressionString.matches(".*[\\.\\,\\-\\+\\*\\^\\/] *[\\.\\,\\-\\+\\*\\^\\/].*")) {
		
			System.out.println("You pass operand or wrote incorrect format of decimal operand.");
			
			return "";
		}
		
		//check opened/closed brackets count
		if(expressionString.chars().filter(ch -> ch == ')').count() != expressionString.chars().filter(ch -> ch == '(').count()){
			
			System.out.println("You pass bracket.");
			
			return "";
		}
		
		//check closed brackets positions
		if(expressionString.matches(".*[\\.\\,\\-\\+\\*\\^\\/] *[)] *[\\.\\,\\-\\+\\*\\^\\/].*")) {
			
			System.out.println("You pass two operands or wrote unnecessay \")\" bracket.");
			
			return "";
		}
		
		//check opened brackets positions
		if(expressionString.matches(".*[\\.\\,\\-\\+\\*\\^\\/] *[(] *[\\.\\,*\\^\\/].*")) {
			
			System.out.println("You pass two operands or wrote unnecessay \"(\" bracket.");
			
			return "";
		}
		
		//check begin of string
		if(expressionString.startsWith("*") ||
			expressionString.startsWith("/") || 
			expressionString.startsWith("^") || 
			expressionString.startsWith(")")){
			
			System.out.println("Expression can begin from \"(\" or \"+\" or \"-\" or digit");
			
			return "";
		}
		
		//check end of string
		if(!(expressionString.endsWith(")") || 
			(Character.isDigit(expressionString.charAt(expressionString.length()-1))))) {
			
			System.out.println("Expression can end with digit or \")\".");
			
			return "";
		} 
		
		//transform expression string if in begin negative number
		if(expressionString.startsWith("+") || expressionString.startsWith("-")){

			expressionString = new StringBuilder(expressionString).insert(0, "0").toString();
		}
		
		//transform expression string if negative number in brackets
		while(expressionString.contains("(+") || expressionString.contains("(-")){
		
			if(expressionString.indexOf("(+") != -1){
					
				expressionString = new StringBuilder(expressionString).insert(expressionString.indexOf("(+") + 1, "0").toString();
			}
				
			if(expressionString.indexOf("(-") != -1) {
					
				expressionString = new StringBuilder(expressionString).insert(expressionString.indexOf("(-") + 1, "0").toString();
			}		
		}
		
		//remove spaces
		expressionString = expressionString.replaceAll(" ", "");
		//replace "," by "."
		expressionString = expressionString.replaceAll(",", ".");
		
		return expressionString;
	}
	
	public static Queue<String> getInfixNotation(String expressionString){
		
		Queue<String> infixNotationQueue = new LinkedList<String>();
		String[] infixNotationStrings = expressionString.split(splitStringRegexp);
		//convert to String array java 8
		infixNotationQueue = new LinkedList<String>(Arrays.asList(infixNotationStrings));
		
		return infixNotationQueue;
	}
	
	//Dejkstra shunting-yard algorithm to convert from 
	//infix notation to reverse polish notation
	public static Queue<String> shuntingYard(Queue<String> infixNotationQueue){
				
		Queue<String> RPNqueue = new LinkedList<String>();
		Stack<String> operatosStack = new Stack<String>();
		
		Map <String, Integer> operatorsPriotiryMap = new HashMap<String, Integer>();
		
		operatorsPriotiryMap.put("^", 3);
		operatorsPriotiryMap.put("*", 2);
		operatorsPriotiryMap.put("/", 2);	
		operatorsPriotiryMap.put("+", 1);
		operatorsPriotiryMap.put("-", 1);
		
		String token;

		while(infixNotationQueue.size() != 0){
			
			token = infixNotationQueue.poll();

			if(token.matches(floatingPointRegexp)) { 
				
				RPNqueue.add(token);
				
			} else
				
			if(token.matches("(-|\\+|\\*|\\/|\\^)")) {
								
				while(operatosStack.size() != 0 && operatosStack.peek().matches("(-|\\+|\\*|\\/|\\^)")){
					
					//token priority =< op2 priority
					Integer tokenPriority = operatorsPriotiryMap.get(token);
					Integer op2Priority = operatorsPriotiryMap.get(operatosStack.peek());
				
					if(tokenPriority <= op2Priority){
							
						RPNqueue.add(operatosStack.pop());
						
					} else {
							
						break;
					}
				}	
				
				
				operatosStack.add(token);
				
			} else	
			
			if(token.equals("(")) {
				
				operatosStack.add(token);
	
			} else
			
			if(token.equals(")")) {
							
				while(operatosStack.size() != 0 && !operatosStack.peek().equals("(")){
						
					RPNqueue.add(operatosStack.pop());
				}
					
				if(!operatosStack.peek().equals("(")){
						
					System.out.println("You pass open bracket.");	
					RPNqueue.clear();
					
					break;
					
				} else {
						
					operatosStack.pop();
				}	
					
			} else {
							
				System.out.println("Error token:" + token);
				RPNqueue.clear();
				
				break;
			}
		}
		
		//end of tokens
		while(operatosStack.size() != 0){
			
			if(operatosStack.peek().matches("(\\(|\\))")){
				
				System.out.println("Pased closed bracket.");
				RPNqueue.clear();
				
				break;
				
			} else {
				
				RPNqueue.add(operatosStack.pop());
			}
		}
		
		return RPNqueue;
	} 
	
	//calculating result, represented in reverse polish notation
	public static String calculateRPN(Queue<String> RPNQueue) {
		
		Stack<String> operndsStack = new Stack<String>();
		String token;
		Double op1;
		Double op2;
		Double stackResult;
		while(RPNQueue.size() != 0){
			
			token = RPNQueue.poll();
			
			if(token.matches(floatingPointRegexp)){
				
				operndsStack.add(token);

		
			} else if(token.matches("(-|\\+|\\*|\\/|\\^)")){
				
				switch(token) {
				
				case "+":
					
					String op1ad = operndsStack.pop();
					String op2ad = operndsStack.pop();
					String additionRes = addition(op1ad, op2ad);
					
					if(additionRes.length() != 0) {
						
						operndsStack.add(additionRes);
						
					} else {
						
						return "";
					}			
					
					break;
					
				case "-":
					
					String op1sub = operndsStack.pop();
					String op2sub = operndsStack.pop();
					String subtrRes = subtraction(op2sub, op1sub);
					
					if(subtrRes.length() != 0){
						
						operndsStack.add(subtrRes);
						
					} else {
						
						return "";
					}
				
					break;
					
				case "*":
					
					String op1mul = operndsStack.pop();
					String op2mul = operndsStack.pop();
					String multRes = multiplication(op1mul, op2mul);
					
					if(multRes.length() != 0) {
						
						operndsStack.add(multRes);
						
					} else {
						
						return "";
					}
						
					break;
				
				//devision by zero can check only here, because expression can be 5/(6-6..?) or more difficult
				case "/":
					
					String op1dev = operndsStack.pop();
					String op2dev = operndsStack.pop();
					String devRes = division(op2dev, op1dev);
					
					if(devRes.length() != 0) {
						
						operndsStack.add(devRes);
						
					} else {
						
						return "";
					}
								
					break;
					
				case "^":
					
					String op1exp = operndsStack.pop();
					String op2exp = operndsStack.pop();
					String expRes = exponentiation(op2exp, op1exp);
					
					if(expRes.length() != 0) {
						
						operndsStack.add(expRes);
						
					} else {
						
						return "";
					}
										
					break;
					
				default:
					
					System.out.println("Error token:" + token);
					
					return "";
				}
				
			} else {
				
				System.out.println("Error token:" + token);
				
				return "";
			}
		}
		
		return operndsStack.pop();
	}
	
	//METHODS TO PERFORM MATHEMATICAL OPERATIONS
	
	//1 - integer
	//2 - big integer
	//3 - double
	//4 - big decimal
	public static int expressionTypeIdentification(String operand1, String operand2){
		
		//minus sign is transformed
		if(!(operand1.contains(".") || operand2.contains("."))) {
				
			if(isInteger(operand1) && isInteger(operand2)){
					
				return 1;
					
			} else {

				return 2;
			}		
			
		} else {
						
			if(!(Double.valueOf(operand1).isInfinite() || Double.valueOf(operand2).isInfinite())){
					
				return 3;
				
			} else {
				
				System.out.println("super big double.");
				
				return 4;			
			}
		}
	}
	
	public static boolean isInteger(String string){
		
		try {
			
	        Integer.valueOf(string);
	        
	        return true;
	        
	    } catch (NumberFormatException e) {
	    	
	        return false;
	    }
	}
	
	//+
	public static String addition(String operand1, String operand2){	
		
		String result = "";
		
		switch(expressionTypeIdentification(operand1, operand2)){
		
		//integer
		case 1:
			
			long addResult = (long)Integer.valueOf(operand1) + Integer.valueOf(operand2);
			
			if(!(addResult > Integer.MAX_VALUE || addResult < Integer.MIN_VALUE)) {
				
				result = String.valueOf(addResult);
				
				break;
				
			} 
			
		//big integer	
		case 2: 
			
			BigInteger operand1BigInt = new BigInteger(operand1);
			BigInteger operand2BigInt = new BigInteger(operand2);
			BigInteger resultBigInt = operand1BigInt.add(operand2BigInt);
			
			result = resultBigInt.toString();
			
			break;
			
		//double	
		case 3:
			
			Double resultDouble = Double.valueOf(operand1) + Double.valueOf(operand2);
			
			if(!Double.isInfinite(resultDouble)) {
				
				result = resultDouble.toString();
				result = checkDoubleRounding(result);
				
				break;
			} 
			
		//big decimal	
		case 4: 
			
			BigDecimal operand1BigDec = new BigDecimal(operand1); 
			BigDecimal operand2BigDec = new BigDecimal(operand2); 
			BigDecimal resultBigDec = operand1BigDec.add(operand2BigDec);
			
			result = resultBigDec.toString();
			result = checkDoubleRounding(result);
			
			break;	
			
		default:
			
			System.out.println("Error in expression type identification.");
			
			break;
		}
		
		return result;
	}
	
	//-
	public static String subtraction(String operand1, String operand2){
		
		String result = "";
		
		switch(expressionTypeIdentification(operand1, operand2)){
		
		//integer
		case 1:
			
			long subtrResult = (long)Integer.valueOf(operand1) - Integer.valueOf(operand2);
			
			if(!(subtrResult > Integer.MAX_VALUE || subtrResult < Integer.MIN_VALUE)) {
				
				result = String.valueOf(subtrResult);
				
				break;
				
			} 
			
		//big integer	
		case 2: 
			
			BigInteger operand1BigInt = new BigInteger(operand1);
			BigInteger operand2BigInt = new BigInteger(operand2);
			BigInteger resultBigInt = operand1BigInt.subtract(operand2BigInt);
			
			result = resultBigInt.toString();
			
			break;
			
		//double	
		case 3:
			
			Double resultDouble = Double.valueOf(operand1) - Double.valueOf(operand2);
			if(!Double.isInfinite(resultDouble)) {
				
				result = resultDouble.toString();
				result = checkDoubleRounding(result);
				
				break;
			} 
			
		//big decimal	
		case 4: 
			
			BigDecimal operand1BigDec = new BigDecimal(operand1); 
			BigDecimal operand2BigDec = new BigDecimal(operand2); 
			BigDecimal resultBigDec = operand1BigDec.subtract(operand2BigDec);
			
			result = resultBigDec.toString();
			result = checkDoubleRounding(result);
			
			break;	
			
		default:
			
			System.out.println("Error in expression type identification.");
			
			break;
		}
		
		return result;
	}
	
	//*
	public static String multiplication(String operand1, String operand2) {
		
		String result = "";
		
		switch(expressionTypeIdentification(operand1, operand2)){
		
		//integer
		case 1:
			
			long addResult = (long)Integer.valueOf(operand1) * Integer.valueOf(operand2);
			
			if(!(addResult > Integer.MAX_VALUE || addResult < Integer.MIN_VALUE)) {
				
				result = String.valueOf(addResult);
				
				break;
				
			} 
			
		//big integer	
		case 2: 
			
			BigInteger operand1BigInt = new BigInteger(operand1);
			BigInteger operand2BigInt = new BigInteger(operand2);
			BigInteger resultBigInt = operand1BigInt.multiply(operand2BigInt);
			
			result = resultBigInt.toString();
			
			break;
			
		//double	
		case 3:
			
			Double resultDouble = Double.valueOf(operand1) * Double.valueOf(operand2);
			
			if(!Double.isInfinite(resultDouble)) {
				
				result = resultDouble.toString();
				result = checkDoubleRounding(result);
				
				break;
			} 
			
		//big decimal	
		case 4: 
			
			BigDecimal operand1BigDec = new BigDecimal(operand1); 
			BigDecimal operand2BigDec = new BigDecimal(operand2); 
			BigDecimal resultBigDec = operand1BigDec.multiply(operand2BigDec);
			
			result = resultBigDec.toString();
			result = checkDoubleRounding(result);
			
			break;	
			
		default:
			
			System.out.println("Error in expression type identification.");
			
			break;
		}
		
		return result;
	}
	
	//\
	public static String division(String operand1, String operand2) {
		
		String result = "";
		
		try{
			switch(expressionTypeIdentification(operand1, operand2)){
			//integer
			//isInteger("2.0") return false - next operation will create double for this number
			case 1:
	
				Integer operand1Int = Integer.valueOf(operand1);
				Integer operand2Int = Integer.valueOf(operand2);
				
				if(operand2Int != 0) {
					
					if(operand1Int % operand2Int == 0) {
						
						Integer devResult =  operand1Int/operand2Int;
						result = String.valueOf(devResult);
							
					} else {
							
						Double devResult =  (double)operand1Int/operand2Int;
						result = String.valueOf(devResult);
					}
					
					break;
				} else {
					
					System.out.println("Devision by zero.");
					
					return "";
				}	
				
			//big integer	
			case 2: 
					
				BigInteger operand1BigInt = new BigInteger(operand1);
				BigInteger operand2BigInt = new BigInteger(operand2);
				
				if(!(operand2BigInt.compareTo(BigInteger.ZERO) == 0)){
					
					if(operand1BigInt.remainder(operand2BigInt).compareTo(BigInteger.ZERO) == 0){
						
						BigInteger resultBigInt = operand1BigInt.divide(operand2BigInt);	
						result = resultBigInt.toString();
							
					} else {
							
						BigDecimal operand1BigDec = new BigDecimal(operand1);
						BigDecimal operand2BigDec = new BigDecimal(operand2);
						
						// can be result 1/3 = 0.333(3) and must restrict precision
						BigDecimal resultBigDec = operand1BigDec.divide(operand2BigDec, MathContext.DECIMAL128);
							
						result = resultBigDec.toString();
					}
					
				} else {
					
					System.out.println("Devision by zero.");
					
					return "";
				}	
											
				break;
			//double	
			case 3:
				
				Double operand1Double = Double.valueOf(operand1);
				Double operand2Double = Double.valueOf(operand2);
				
				if(operand2Double != 0){
					
					Double resultDouble = operand1Double/operand2Double;
					
					if(!Double.isInfinite(resultDouble)) { // can be infinity because can divide by value 0..1
							
						result = resultDouble.toString();
						result = checkDoubleRounding(result);
							
						break;
					} 
					
				}else {
					
					System.out.println("Devision by zero.");
					
					return "";
				}	
				
			//big decimal	
			case 4: 
					
				BigDecimal operand1BigDec = new BigDecimal(operand1); 
				BigDecimal operand2BigDec = new BigDecimal(operand2); 
				
				if(!(operand2BigDec.compareTo(BigDecimal.ZERO) == 0)){
					
					// can be result 1/3 = 0.333(3) and must restrict precision
					BigDecimal resultBigDec = operand1BigDec.divide(operand2BigDec, MathContext.DECIMAL128);
						
					result = resultBigDec.toString();
					result = checkDoubleRounding(result);
						
					break;	
					
				} else {
					
					System.out.println("Devision by zero.");
					
					return "";
				}
				
					
			default:
					
				System.out.println("Error in expression type identification.");
					
				break;
			}
			
		} catch(ArithmeticException e){
				
			System.out.println("Error: devision by zero.");
			
			return "";
		}		
		
		return result;
	}
	
	//^
	public static String exponentiation(String operand1, String operand2) {
		
		String result = "";
		
		if(isInteger(operand2)) {
			
			switch(expressionTypeIdentification(operand1, operand2)){
		
			//big integer	
			case 2: 
				
				BigInteger operand1BigInt = new BigInteger(operand1);
				Integer operand2Int = Integer.valueOf(operand2);
		
				try {
					
					BigInteger resultBigInt = operand1BigInt.pow(operand2Int); // check arithmetic exception			
					result = resultBigInt.toString();
					
				}catch(ArithmeticException e) {
					
					System.out.println("Problems with calculation. JVM restrictions. Presumably to large exponent value.");
					
					return "";
				}			
				break;
			//integer or double because Math.pow require double arguments and return double result
			case 1:
			case 3:
				
				Double resultDouble = Math.pow(Double.valueOf(operand1), Double.valueOf(operand2));
				
				if(!Double.isInfinite(resultDouble)) {
					
					result = resultDouble.toString();
					result = checkDoubleRounding(result);
					
					break;
				} 
			//big decimal	
			case 4: 
				
				BigDecimal operand1BigDec = new BigDecimal(operand1); 
				Integer operand2IntD = Integer.valueOf(operand2); 
					
				try {
						
					BigDecimal resultBigDec = operand1BigDec.pow(operand2IntD);
					result = resultBigDec.toString();
					result = checkDoubleRounding(result);
						
				}catch(ArithmeticException e) {
						
					System.out.println("Problems with calculation. JVM restrictions. Presumably to large exponent value.");
					System.out.println("No need to calculate pow(n) if result will over/underflow.(c)");
					System.out.println("Suggest restrict exponent in range 0..999999999 for this case (Big decimal).");
						
					return "";
				}				
				
				break;	
				
			default:
				
				System.out.println("Error in expression type identification.");
				
				break;
			}
			
		} else {
			
			System.out.println("Error in expression operand " + operand2);
			System.out.println("Exponent must be integer in range -2147483648 .. 2147483647).");
		}
			
		return result;
	}
	
	//two double operators can return integer value: 1,5 + 1,5 = 3...
	public static String checkDoubleRounding(String doubleResultString){
		
		if(doubleResultString.matches("([0-9]*[.])?[0]+")) {
			
			doubleResultString = doubleResultString.split("\\.")[0];

		} 
		
		return doubleResultString;
	}
}
