package task2;

import java.math.BigDecimal;

public class TestBigDecimal {
	
	public static void main(String[] args) {
		Double k = 3.5;
		Double z = 4.5;
		Double res = k + z;
		String resDouble = res.toString();
		System.out.println(resDouble);
		String doublePow = String.valueOf(Math.pow(k, 0)); 
		System.out.println(doublePow);
		BigDecimal bigDec1 = new BigDecimal("1234.999999999");
		BigDecimal bigDec2 = new BigDecimal("1234.000000001");
		BigDecimal bigDec3 = bigDec1.add(bigDec2);
		String resBigDec = bigDec3.toString();
		System.out.println(resBigDec);
		String resBigDecPow = bigDec1.pow(0).toString();
		System.out.println(resBigDecPow);
		
		System.out.println("Modification:");
		System.out.println(checkDoubleRounding(resDouble));
		System.out.println(checkDoubleRounding(doublePow));
		System.out.println(checkDoubleRounding(resBigDec));
		System.out.println(checkDoubleRounding(resBigDecPow));
		System.out.println(checkDoubleRounding("1234.456"));
	}
	
	public static String checkDoubleRounding(String doubleResultString){
		
		if(doubleResultString.matches("([0-9]*[.])?[0]+")) {
			
			System.out.println("Must round:");
			doubleResultString = doubleResultString.split("\\.")[0];

		} 
		
		return doubleResultString;
	}
}
