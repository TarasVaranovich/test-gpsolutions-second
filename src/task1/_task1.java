package task1;

import java.util.Scanner;

import java.util.Queue;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Set;
import java.util.HashSet;

public class _task1 {
	
	public static void main(String[] args) {
		
		System.out.println("Task1 program.");
		String predeterminedString = " ";	
		String parameterString = "";

		Scanner userInString = new Scanner(System.in);
		Scanner userInParameter = new Scanner(System.in);
		
		Queue<String> stringsQueue = new LinkedList<String>();
		// array of strings - queue
		//if function add to gueue or queue and than filter by dequeue
		
		System.out.println("Enter a parameter word or words, if more than one, devided by space.");
		parameterString = userInParameter.nextLine();
		System.out.println("Parameter word(s): " + parameterString);
		System.out.println("Input text. Empty string means end of text input.");
		
		do{
			
			//System.out.println("Enter a line of text:\n");
			predeterminedString = userInString.nextLine();
			
			if(predeterminedString.length() != 0) {
				
				//System.out.println("You entered: " + predeterminedString);
				stringsQueue.add(predeterminedString);
			}
			
		} while(predeterminedString.length() != 0);
		
		System.out.println("Result, filtered by parameter:\n");
		
		while(stringsQueue.size() != 0){
			
			String stringToPrint =  stringsQueue.poll();
			
			if(checkString(parameterString, stringToPrint)){
				
				System.out.println(stringToPrint);
				
			} 
		}
		
		System.out.println("Exit from Task1 program.");
		System.exit(0);
	}
	
	private static boolean checkString(String parameterString, String predeterminedString){
		//самые длительные  - операции поиска. Поиск пробелов займет то же время что и разделение по пробелам
		
		String parameterStrings [] = parameterString.split(" ");
		String predeterminedStrings[] = predeterminedString.split(" ");
		
		Set<String> parametersSet = new HashSet<String>(Arrays.asList(parameterStrings));
		Set<String> predeterminedSet = new HashSet<String>(Arrays.asList(predeterminedStrings));
		predeterminedSet.retainAll(parametersSet);
		String str1 = "";
		for(String str: predeterminedSet){
			str1.concat(":");
			str1.concat(str);
		}
		System.out.println(">" + str1);
		//

		return true;
	}
	//matches нужно все конвертить в регулярные выражения
	//set.filter (matches)
}
