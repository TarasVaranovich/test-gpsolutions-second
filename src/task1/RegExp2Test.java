package task1;

public class RegExp2Test {
	
	public static void main(String[] args) {
		
		String string = "sjkdkjsk gde-abcd jkjfkgkf;";
		
		String matcher = "abc";
		if(string.contains(matcher.concat(" ")) || 
				string.contains(" ".concat(matcher).concat(" ")) || 
				string.contains(" ".concat(matcher).concat(";"))) {
			
			System.out.println("contains");
		}
		
		String secondString = "sdlsklgfkklhkglhh ;";
		if(secondString.endsWith(";")) {
			System.out.println("true");
		}
		
		//MATCHERS
		String param = "gde-abc";
		if(string.matches("^".concat(param).concat(" .*"))){
			System.out.println("Matches in begin");
		}
		
		if(string.matches(".* ".concat(param).concat(";$"))) {
			System.out.println("Matches in end");
		}
		
		if(string.matches("^".concat(param).concat(";$"))) {
			System.out.println("Match if only one.");
		}
		
		if(string.matches(".* ".concat(param).concat(" .*"))){
			System.out.println("Matches in center.");
		}
		
		String commonExpression = "^".concat(param).concat(" .*") + "|" + 
									".* ".concat(param).concat(";$") + "|" + 
									"^".concat(param).concat(";$") +"|" + 
									".* ".concat(param).concat(" .*");
		
		if(string.matches(commonExpression)){
			
			System.out.println("Matches common");
		}
	}
}
