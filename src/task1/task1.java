package task1;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class task1 {
	
	//private static final String openMarker = "(";
	//private static final String closeMarker = ")";
	private static final String openMarker = "\033[1m";
	private static final String closeMarker = "\033[0m";
	
	public static void main(String[] args) {
	
		System.out.println("Task1 program.");
		Scanner scanner = new Scanner(System.in);
		Set<String> parameters = new HashSet<String>(); // because parameters must be unique
		System.out.println("Enter a parameter word or words, if more than one, devided by space.");
		System.out.println("Empty string means end of text input.");
		String parameterString = "";
		
		do{
			
			parameterString = scanner.nextLine();
			
			if(parameterString.length() != 0) {
				
				String[] parametersArray = parameterString.split(" ");
				//java 8 lambda to trim all members of array
				Arrays.stream(parametersArray).map(String::trim).toArray(string -> parametersArray);
				parameters.addAll(Arrays.asList(parametersArray));
				
			}
			
		} while(parameterString.length() != 0);
		

		Queue<String> stringsQueue = new LinkedList<String>();
		
		System.out.println("Input text. End each string with \";\" sign. Empty string means end of text input.");
		String predeterminedString = "";
		do{
			
			predeterminedString = scanner.nextLine();
			
			if(predeterminedString.length() != 0) {
				
				if(predeterminedString.endsWith(";")) {
					
					stringsQueue.add(predeterminedString);
					
				} else {
					
					System.out.println("String must end with sign \";\".");
					System.out.println("Exit from Task1 program.");
					System.exit(0);
				}			
			}
			
		} while(predeterminedString.length() != 0);
		
		System.out.println("Result, filtered by parameter:\n");
		
		while(stringsQueue.size() != 0){
			
			String stringToPrint = stringsQueue.poll();
			String foundCandidateString = checkString(parameters, stringToPrint);
			
			if(foundCandidateString.length() != 0){
				
				System.out.println(foundCandidateString);
			}		
		}
		
		System.out.println("Exit from Task1 program.");
		System.exit(0);
	}
	
	public static String checkString(Set<String> parameters, String stringPred){
		
		String string = stringPred;
		
		boolean found = false;
		
		for(String strPar: parameters){
			
			if(isRegExp(strPar)) {
				
				string = string.substring(0, string.length() - 1);
				
				String[] strings = string.split(" ");
				String stringRes = "";
				
				int currentPos = 0;
				
				boolean foundStr = false; // indicate if in this string already exists regExp
				
				for(int i = 0; i < strings.length; i++){
					
					currentPos = currentPos + strings[i].length();
					
					if(strings[i].matches(strPar)) {

						//adding "i" in end because each next string begin with gap
						// and gaps offset equal i-count
						int startPos = currentPos - strings[i].length() + i;
						string = new StringBuilder(string).insert(startPos, openMarker).toString();
						
						int endPos = startPos + strings[i].length() + openMarker.length();
			 	    	string = new StringBuilder(string).insert(endPos, closeMarker).toString();	
			 	    	
			 	    	currentPos = currentPos + openMarker.length() + closeMarker.length();
			 	    	
			 	    	//remove previous marking
			 	    	if(foundStr){ // if string already found
			 	    		
			 	    		int startPosRem = string.indexOf(openMarker);
				 	    	string = new StringBuilder(string).delete(startPosRem, startPosRem + openMarker.length()).toString();
				 	    	int endPosRem = string.indexOf(closeMarker);
				 	    	string = new StringBuilder(string).delete(endPosRem, endPosRem + closeMarker.length()).toString();
			 	    	}
			 	    	
			 	    	foundStr = true;
						found = true;					
					} 
				}
				
				string = string.concat(";");
				
			} else {
				
				String resultString = markWords(string, strPar);
				
				if(resultString.length() != 0) {
					
					string = resultString;
					found = true;
				}
			}		
		}
		
		if(found) {
			
			return string;
			
		} else {
			
			return "";
		}		
	}
	
	public static String  markWords(String string, String word){
		
		boolean found = false;
		
		String quotedWord = Pattern.quote(word);
		String patternM = "(?i)(?<=\\s|^)"+Pattern.quote(word)+"(?=\\s|;$)";
		
		String pattern = "^".concat(quotedWord).concat(" .*") + "|" + 
				".* ".concat(quotedWord).concat(";$") + "|" + 
				"^".concat(quotedWord).concat(";$") +"|" + 
				".* ".concat(quotedWord).concat(" .*");
		
		
		Pattern p = Pattern.compile(patternM);
	   
		int z = 0;
		
	    while(true){
	    	
	    	 Matcher m = p.matcher(string);
	    	 
	    	 if(string.matches(pattern)){
	    		 
	 	    	m.find(z);

	 	    	string = new StringBuilder(string).insert(m.start(), openMarker).toString();
	 	    	string = new StringBuilder(string).insert(m.end() + openMarker.length(), closeMarker).toString();
	 	    	z = m.end();
	 	    	found = true;
	 	    	
	 	    } else {
	 	    	
	 	    	break;
	 	    }	
	    }
	    
	    if(found) {
	    	
	    	return string;
	    	
	    } else {
	    	
	    	return "";
	    	
	    }   
	}
	
	public static boolean isRegExp(String regexpCandidate){
		
		//word can contain only letters and can contain one hyphen, else it is not word
		// in English can be "number + noun" adjectives, which can contain digits in begin (in case > 10)
		//if number decimal - write without hyphen as number. (In technique in most cases)
		if(!regexpCandidate.matches("[a-zA-Z0-9]+\\.?\\-?[a-zA-Z]+\\.?")){
			
			try{
				
				Pattern regexp = Pattern.compile(regexpCandidate);
				
				return true;
				
			} catch(PatternSyntaxException e) {
				
				return false;
			}
			
		} else {
			
			return false;
		}	
	}
}
