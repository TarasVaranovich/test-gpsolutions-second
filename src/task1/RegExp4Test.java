package task1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExp4Test {

	public static void main(String[] args) {
		//".*".matches(Pattern.quote(".*")
		//
		Pattern p = Pattern.compile("(?i)(?<=\\s|^)"+Pattern.quote("*")+"(?=\\s|;$)");
		Matcher m = p.matcher("as * fg;");
		m.find();
		System.out.println(m.start() + ":"+ m.end());
	}
}
