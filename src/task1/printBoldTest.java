package task1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class printBoldTest {
	
	public static final String floatingPointRegexp = "([0-9]*[.])?[0-9]+";
	
	public static void main(String[] args) {
		String asd = "Bold line";
		String adf = "Thin line";
		//bold text, normal text
		System.out.println("\033[1m"+asd+"\033[0m" + adf);
		
		String string = "fg dfg abc dhjk abc";
		String pattern = ".*abc.*";
		System.out.println("Markered:");
		//System.out.println(markWords("abcd abc abcdf", ".*\\babc\\b.*"));
		String string2 = "as gabc-df fgh ;";
		String word = "gabc-df";
		
		String foundStr = markWords(string2, word);
		
		if(foundStr.length() != 0) {
			
			System.out.println(foundStr);
			
		} else {
			
			System.out.println("No matches...");
		}
		
		String found2Str =  markWords(foundStr, "fgh");
		System.out.println(found2Str);
			   
	}
	
	public static String  markWords(String string, String word){
		
		boolean found = false;
		
		String patternM = "\\b".concat(word).concat("\\b");
		String pattern = "^".concat(word).concat(" .*") + "|" + 
				".* ".concat(word).concat(";$") + "|" + 
				"^".concat(word).concat(";$") +"|" + 
				".* ".concat(word).concat(" .*");
		
		
		Pattern p = Pattern.compile(patternM);
	   
		int z = 0;
		
	    while(true){
	    	
	    	 Matcher m = p.matcher(string);
	    	 
	    	 if(string.matches(pattern)){
	    		 
	 	    	m.find(z);
	 	    	//System.out.println(m.group());
	 	    	//System.out.println(m.start());
	 	    	//System.out.println(m.end());
	 
	 	    	string = new StringBuilder(string).insert(m.start(), "(").toString();
	 	    	string = new StringBuilder(string).insert(m.end() + 1, ")").toString();
	 	    	z = m.end();
	 	    	found = true;
	 	    	
	 	    } else {
	 	    	
	 	    	break;
	 	    }	
	    }
	    
	    if(found) {
	    	
	    	return string;
	    	
	    } else {
	    	
	    	return "";
	    	
	    }   
	}
	
	public static String markWords2(String expressionString, String patternStr){
			
		if(expressionString.matches(patternStr)) {
			System.out.println("Search entries:");
			Pattern pattern = Pattern.compile(patternStr); // here is a failure
			
			while (true) {
				
				Matcher matcher = pattern.matcher(expressionString);
				matcher.find();
				if(matcher.find()){
					
				     expressionString = new StringBuilder(expressionString).insert(matcher.start(), "(").toString();
				     expressionString = new StringBuilder(expressionString).insert(matcher.end()+1, ")").toString();
				     break;
				     
				} else {
					
					break;
				}		    	       
		    }
		} else {
			
			return "No matches.";
		}
			
		return expressionString;
	}
}
