package task1;

import java.util.regex.Pattern;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.Arrays;

public class StringsTest {
	
	public static void main(String[] args) {
		
		String str = "dffddfdfdf";
		
		Set<String> parameters = new HashSet<String>();
		
		String parameter = "I'm very happy in space. Cells system, system of cells; "
															+ "start now; start after. Why?! Because you can.";
		String[] parametersArr = parameter.split(" ");
		parameters.addAll(Arrays.asList(parametersArr));
		
		parameters.addAll(Arrays.asList("bcd 123 # $".split(" ")));
		parameters.addAll(Arrays.asList("pre-define".split(" ")));
		parameters.addAll(Arrays.asList("'*'mre".split(" ")));
		parameters.addAll(Arrays.asList("//<< asd ?>>".split(" ")));
		
		//ркгулярное выражение используется для проверки каждого слова отдельно
		//значит в нем нет симвлов кроме "-"
	}
	
	
	public static boolean checkString(Set<String> parameters, String string){
		
		for(String strPar: parameters){
			
			if(isRegExp(strPar)) {
				
				String[] strings = string.split(" ");
				
				for(int i = 0; i < strings.length; i++){
					if(strings[i].matches(strPar)) {
						//true
						return true;
					}
				}
				
			} else {
				
				if(string.contains(strPar)){
					
					return true;
				}
			}
			
			
		}
		
		return false;
	}
	
	public static boolean isRegExp(String regexpCandidate){
		
		try{
			
			Pattern regexp = Pattern.compile(regexpCandidate);
			
			return true;
			
		} catch(Exception e) {
			
			return false;
		}
	}
}
