package task1;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class RegExpTest {
	
	public static void main(String[] args) {
		
		System.out.println(isRegExp("*"));
		System.out.println(isRegExp("44abcd"));
		System.out.println(isRegExp("24-dors"));
		System.out.println(isRegExp("245")); // "decimal is the same"
		System.out.println(isRegExp("abcd"));
		System.out.println(isRegExp("abc"));
		System.out.println(isRegExp("^ab.+"));
		System.out.println(isRegExp("semi-count"));
		System.out.println(isRegExp("semi--count"));
		System.out.println(isRegExp("2,5-count"));
		System.out.println("M:" + "ab".matches("^ab.+"));
		
	}
	
	public static boolean isRegExp(String regexpCandidate){
		
			//word can contain only letters and can contain on hyphen between else it is not word
			// in English can be number + noun adjectives, which can contain digits in begin (in case  >10)
			//if number decimal - write without hyphen as number. (In technique in most cases)
			if(!regexpCandidate.matches("[a-zA-Z0-9]+\\.?\\-?[a-zA-Z]+\\.?")){
				
				try{
					
					Pattern regexp = Pattern.compile(regexpCandidate);
					
					return true;
					
				} catch(PatternSyntaxException e) {
					
					return false;
				}
				
			} else {
				
				return false;
			}
		
	}
}
