package task1;

public class StringBuilderTest {
	public static void main(String[] args) {
		String string = "My win is great";
		string = new StringBuilder(string).insert(3, "go-go-go!").toString();
		string = new StringBuilder(string).insert(6, "go-go-go!").toString();
		System.out.println(string);
	}
}
